package core;

import sun.rmi.runtime.Log;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
    private String type;
    private String content;
    private String hash;

    public String getHash() {
        return hash;
    }

    public GitBlobObject(String repoPath, String s) {
        String subPathOne, subPathTwo, absPath;
        hash = s;


        absPath = repoPath + "/objects";
        subPathOne = s.substring(0, 2);
        subPathTwo = s.substring(2);
        absPath = absPath+"/"+subPathOne+"/"+subPathTwo;

        try (FileInputStream fs = new FileInputStream(absPath)){

            InflaterInputStream is = new InflaterInputStream(fs);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String contenutoFile = "";
            int value = 0;

            // reads to the end of the stream
            while((value = br.read()) != -1) {

                // converts int to character
                char c = (char)value;

                // prints character
                contenutoFile += c;

            }

            System.out.print(contenutoFile);

            this.type = contenutoFile.split("\u0000")[0].split(" ")[0];
            this.content = contenutoFile.split("\u0000")[1];

            System.out.println("debug");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}

package core;

public class GitCommitObject {
    private String hash;
    private String masterCommitHash;
    private String treeHash;
    private String parentHash;
    private String author;

    public GitCommitObject(String repoPath, String masterCommitHash) {
        hash = masterCommitHash;
        this.masterCommitHash = masterCommitHash;

        GitBlobObject blob = new GitBlobObject(repoPath, masterCommitHash);
        String blobContent = blob.getContent();

        String[] splittedContent = blobContent.split("\n");

        treeHash = splittedContent[0].substring(5);
        parentHash = splittedContent[1].substring(7);
        String[] authorInfos = splittedContent[2].substring(7).split(" ");
        author = authorInfos[0]+" "+authorInfos[1]+ " "+authorInfos[2];

    }

    public String getHash() {
        return hash;
    }

    public String getTreeHash() {
        return treeHash;
    }

    public String getParentHash() {
        return parentHash;
    }

    public String getAuthor() {
        return author;
    }
}

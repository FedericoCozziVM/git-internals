package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {
    private String path;

    public GitRepository(String path) {
        this.path = path;
    }

    public String getHeadRef() {

        try (BufferedReader bf = new BufferedReader(new FileReader(path + "/HEAD"))) {
            return bf.readLine().substring(5);
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (IOException f) {
            f.printStackTrace();
        }

        return "";

    }

    public String getRefHash(String s) {
        try (BufferedReader bf = new BufferedReader(new FileReader(path + "/refs/heads/master"))) {
            return bf.readLine();
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (IOException f) {
            f.printStackTrace();
        }
        return "";
    }
}